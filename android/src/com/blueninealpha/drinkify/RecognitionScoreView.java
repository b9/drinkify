/* Copyright 2015 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package com.blueninealpha.drinkify;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import com.blueninealpha.drinkify.Classifier.Recognition;

import java.util.List;

public class RecognitionScoreView extends View implements ResultsView {
  private static final float TEXT_SIZE_DIP = 24;
  private List<Recognition> results;
  private final float textSizePx;
  private final Paint fgPaint;
  private final Paint bgPaint;
  private final Paint titlePaint;
  private ResultsAggregator aggregator = new ResultsAggregator();

  public RecognitionScoreView(final Context context, final AttributeSet set) {
    super(context, set);

    textSizePx =
        TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
    fgPaint = new Paint();
    fgPaint.setTextSize(textSizePx);
    fgPaint.setColor(0xffdddddd);
    AssetManager assetManager = context.getApplicationContext().getAssets();
    Typeface plain = Typeface.createFromAsset(assetManager, "SakerSansMedium.ttf");
    fgPaint.setTypeface(plain);

    titlePaint = new Paint();
    titlePaint.setTextSize(new Float(textSizePx * 1.4));
    titlePaint.setColor(0xffdddddd);
    titlePaint.setTypeface(plain);

    bgPaint = new Paint();
    bgPaint.setColor(0xfff47421);
  }

  @Override
  public void setResults(final List<Recognition> results) {
    aggregator.push(results);
    postInvalidate();
  }

  private String capitalize(String source) {
    StringBuffer res = new StringBuffer();
    String[] strArr = source.split(" ");
    for (String str : strArr) {
      char[] stringArray = str.trim().toCharArray();
      stringArray[0] = Character.toUpperCase(stringArray[0]);
      str = new String(stringArray);

      res.append(str).append(" ");
    }
    return res.toString().trim();
  }

  @Override
  public void onDraw(final Canvas canvas) {
     int y = (int) (fgPaint.getTextSize() * 2.0f);

    int buffer = canvas.getWidth() / 10;
    canvas.drawPaint(bgPaint);
    canvas.drawText("Find a drink to analyze!", buffer * 2, y, titlePaint);
    y += fgPaint.getTextSize() * 2f;

    List<ResultsAggregator.AggregatedResult> results = aggregator.getResults();
    for (int i = 0; i < 4 && i < results.size(); i++) {
      ResultsAggregator.AggregatedResult result = results.get(i);
      canvas.drawText(capitalize(result.name), buffer, y, fgPaint);
      canvas.drawRect((canvas.getWidth() - buffer) - (result.probability * canvas.getWidth() / 2), y - fgPaint.getTextSize(), canvas.getWidth() - buffer, y, fgPaint);
      y += fgPaint.getTextSize() * 1.5f;
    }
  }
}
