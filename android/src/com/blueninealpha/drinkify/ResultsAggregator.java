package com.blueninealpha.drinkify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by johan on 02/11/2017.
 */

public class ResultsAggregator {


    private LinkedList<List<Classifier.Recognition>> pastResults = new LinkedList<>();
    private final int MAX_HISTORY = 30;

    public void push(final List<Classifier.Recognition> results) {
        pastResults.offerFirst(results);
        if(pastResults.size() > MAX_HISTORY) {
            pastResults.pollLast();
        }
    }

    public List<AggregatedResult> getResults() {

        Map<String, Float> confidenceMap = new HashMap();

        // Add all up
        for(List<Classifier.Recognition> results : pastResults) {
            for (final Classifier.Recognition recog : results) {
                Float value = confidenceMap.get(recog.getTitle());
                float floatValue = 0;
                if(value != null) {
                    floatValue += value.doubleValue();
                }
                confidenceMap.put(recog.getTitle(), floatValue + recog.getConfidence());
            }
        }

        List<AggregatedResult> aggregatedValues = new ArrayList<AggregatedResult>();

        for(String key : confidenceMap.keySet()) {
            aggregatedValues.add(new AggregatedResult(key, confidenceMap.get(key) / MAX_HISTORY));
        }
        Collections.sort(aggregatedValues, (a, b) -> new Float(b.probability).compareTo(new Float(a.probability)));
        return aggregatedValues;
    }



    public class AggregatedResult {
        public final String name;
        public final float probability;
        public AggregatedResult(String name, float probability) {
            this.name = name;
            this.probability = probability;
        }
    }

}
