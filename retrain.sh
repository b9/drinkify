#!/usr/bin/env bash

ARCHITECTURE=mobilenet_0.50_224

# Retrain
python -m scripts.retrain   --bottleneck_dir=tf_files/bottlenecks   --how_many_training_steps=5000   --model_dir=tf_files/models/   --summaries_dir=tf_files/training_summaries/"${ARCHITECTURE}"   --output_graph=tf_files/retrained_graph.pb   --output_labels=tf_files/retrained_labels.txt   --architecture="${ARCHITECTURE}"   --image_dir=tf_files/drinks

# Optimize for just output nodes and merge operations. Speed up about 30%
python -m tensorflow.python.tools.optimize_for_inference   --input=tf_files/retrained_graph.pb   --output=tf_files/optimized_graph.pb   --input_names="input"   --output_names="final_result"

# Quantizes the constants to get better compression
python -m scripts.quantize_graph   --input=tf_files/optimized_graph.pb   --output=tf_files/rounded_graph.pb   --output_node_names=final_result   --mode=weights_rounded

mv tf_files/retrained_labels.txt android/assets
mv tf_files/rounded_graph.pb android/assets
