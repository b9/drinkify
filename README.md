# Overview

This repo contains code for the Drinkify Drink Classifier app.

It is based on the ["TensorFlow for poets 2" codelab](https://codelabs.developers.google.com/codelabs/tensorflow-for-poets-2)

In order to retrain it you ned to install TensorFlow and run retrain.sh which will retrain a [MobileNet](https://research.googleblog.com/2017/06/mobilenets-open-source-models-for.html) neural network to detect the supplied drinks.


